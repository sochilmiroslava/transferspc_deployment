﻿const A = [
2.121000051,
2.121000051,
2.121000051,
1.731999993,
1.5,
1.342000008,
1.225000024,
1.133999944,
1.06099999,
1,
0.949000001,
0.904999971,
0.865999997,
0.832000017,
0.801999986,
0.774999976,
0.75,
0.727999985,
0.707000017,
0.688000023,
0.671000004,
0.654999971,
0.639999986,
0.625999987,
0.611999989,
0.600000024];

const A2 = [
1.879999995,
1.879999995,
1.879999995,
1.023000002,
0.728999972,
0.577000022,
0.48300001,
0.419,
0.372999996,
0.337000012,
0.307999998,
0.284999996,
0.266000003,
0.248999998,
0.234999999,
0.223000005,
0.211999997,
0.202999994,
0.194000006,
0.187000006,
0.180000007,
0.172999993,
0.166999996,
0.162,
0.157000005,
0.152999997
];

const A3 = [
    2.65899992,
2.65899992,
2.65899992,
1.953999996,
1.628000021,
1.427000046,
1.286999941,
1.182000041,
1.098999977,
1.031999946,
0.975000024,
0.926999986,
0.885999978,
0.850000024,
0.816999972,
0.788999975,
0.763000011,
0.739000022,
0.717999995,
0.698000014,
0.680000007,
0.662999988,
0.647000015,
0.633000016,
0.619000018,
0.606000006
];

const C4 = [
    0.797900021,
0.797900021,
0.797900021,
0.886200011,
0.921299994,
0.939999998,
0.951499999,
0.959399998,
0.964999974,
0.969299972,
0.9727,
0.975399971,
0.977599978,
0.979399979,
0.981000006,
0.982299984,
0.983500004,
0.984499991,
0.985400021,
0.986199975,
0.986899972,
0.987600029,
0.988200009,
0.988699973,
0.989199996,
0.989600003
];

const B3 = [
    0,
0,
0,
0,
0,
0,
0.029999999,
0.118000001,
0.185000002,
0.238999993,
0.284000009,
0.32100001,
0.354000002,
0.381999999,
0.405999988,
0.428000003,
0.448000014,
0.465999991,
0.481999993,
0.497000009,
0.50999999,
0.523000002,
0.533999979,
0.545000017,
0.555000007,
0.564999998
];

const B4 = [
    3.26699996,
3.26699996,
3.26699996,
2.568000078,
2.266000032,
2089,
1.970000029,
1.881999969,
1.815000057,
1.761000037,
1.715999961,
1.67900002,
1.646000028,
1.618000031,
1.593999982,
1.572000027,
1.552000046,
1.534000039,
1.518000007,
1.503000021,
1.49000001,
1.476999998,
1.465999961,
1.455000043,
1.445000052,
1.434999943
];

const d2 = [
    1.128000021,
1.128000021,
1.128000021,
1.692999959,
2.059000015,
2.325999975,
2.53399992,
2.703999996,
2.846999884,
2.970000029,
3.078000069,
3.173000097,
3.257999897,
3.335999966,
3.407000065,
3.471999884,
3.532000065,
3.588000059,
3.640000105,
3.688999891,
3.734999895,
3.778000116,
3.819000006,
3.85800004,
3.894999981,
3.930999994
];

const B5 = [
    0,
0,
0,
0,
0,
0,
0.028999999,
0.112999998,
0.179000005,
0.231999993,
0.275999993,
0.312999994,
0.345999986,
0.374000013,
0.398999989,
0.421000004,
0.439999998,
0.458000004,
0.474999994,
0.49000001,
0.504000008,
0.515999973,
0.527999997,
0.538999975,
0.549000025,
0.559000015
];

const B6 = [
    2.605999947,
2.605999947,
2.605999947,
2.276000023,
2.088000059,
1.963999987,
1.873999953,
1.805999994,
1.751000047,
1.707000017,
1.66900003,
1.636999965,
1.610000014,
1.585000038,
1.562999964,
1.54400003,
1.526000023,
1.511000037,
1.496000051,
1.48300004,
1.470000029,
1.458999991,
1.447999954,
1.437999964,
1.42900002,
1.419999957
];

const d3 = [
    0.852999985,
0.852999985,
0.852999985,
0.888000011,
0.879999995,
0.864000022,
0.84799999,
0.833000004,
0.819999993,
0.808000028,
0.796999991,
0.787,
0.777999997,
0.769999981,
0.763000011,
0.755999982,
0.75,
0.744000018,
0.739000022,
0.734000027,
0.728999972,
0.723999977,
0.720000029,
0.716000021,
0.712000012,
0.708000004
];

const D1r = [
    0,
0,
0,
0,
0,
0,
0,
0.203999996,
0.388000011,
0.546999991,
0.686999977,
0.81099999,
0.921999991,
1.024999976,
1.118000031,
1.202999949,
1.281999946,
1.355999947,
1.424000025,
1.486999989,
1.549000025,
1.605000019,
1.659000039,
1.710000038,
1.758999944,
1.805999994
];

const D2r = [
    3.686000109,
3.686000109,
3.686000109,
4358,
4.697999954,
4.918000221,
5.078000069,
5.203999996,
5.306000233,
5.393000126,
5.468999863,
5.534999847,
5.593999863,
5.646999836,
5.696000099,
5.741000175,
5.782000065,
5.820000172,
5.855999947,
5.890999794,
5.921000004,
5.951000214,
5979,
6.006000042,
6.031000137,
6.056000233
];

const D3r = [
    0,
0,
0,
0,
0,
0,
0,
0.075999998,
0.136000007,
0.184,
0.223000005,
0.256000012,
0.282999992,
0.307000011,
0.328000009,
0.347000003,
0.363000005,
0.377999991,
0.391000003,
0.402999997,
0.414999992,
0.425000012,
0.433999985,
0.442999989,
0.451000005,
0.458999991
];

const D4r = [
    3.26699996,
3.26699996,
3.26699996,
2.573999882,
2.282000065,
2.114000082,
2.003999949,
1.924000025,
1.863999963,
1.815999985,
1.77699995,
1.743999958,
1.717000008,
1.692999959,
1.672000051,
1.652999997,
1.636999965,
1.621999979,
1.60800004,
1.597000003,
1.585000038,
1.575000048,
1.565999985,
1.557000041,
1.547999978,
1.541000009
];

function CalculateCp(LIE, LSE, DesvEstEst) {

    return (LSE - LIE) / (6 * DesvEstEst);

}

function CalculateCPK(LIE, LSE, Media, DesvEstEst) {

    var Valor1 = (Media - Number(LIE)) / (3 * DesvEstEst);
    var Valor2 = (Number(LSE) - Media) / (3 * DesvEstEst);

    if (Valor1 > Valor2) {
        return Number(Valor2.toFixed(4));
    }
    else {
        return  Number(Valor1.toFixed(4));
    }

}

function CalculateDesEst(Array_Values) {
    var Resultado = 0;
    var Sumatoria = 0;
    var cantidad = 0;
    var Media = CalculateMedia(Array_Values);
    for (var i = 0; i < Array_Values.length; i++) {
        var TempValor = Number(Array_Values[i]) - Media;
        var TempCuadrado = Math.pow(TempValor, 2);
        Sumatoria += Number(TempCuadrado);
        cantidad++;
    }


    Resultado = Sumatoria / cantidad;



    return Math.sqrt(Resultado);
}

function CalculateDesEstEst(Array_Values_Range, TamSubGrupo) {
    var Sumatoria = 0;
    var ValorConstante = d2[TamSubGrupo];


    for (var i = 0; i < Array_Values_Range.length; i++) {
        var CurrentVar = Array_Values_Range[i];
        Sumatoria += Number(CurrentVar);
    }

    var Promedio = (Sumatoria / Array_Values_Range.length).toFixed(8);

    return Promedio / ValorConstante;
}

function CalculateVarianza(Array_Values) {
    var Resultado = 0;
    var Sumatoria = 0;
    var Media = CalculateMedia(Array_Values);
    for (var i = 0; i < Array_Values.length; i++) {
        var TempValor = Number(Array_Values[i]) - Media;
        var TempCuadrado = Math.pow(TempValor, 2);
        Sumatoria += Number(TempCuadrado);
    }


    Resultado = Sumatoria / (i - 1);



    return Resultado;
}

function CalculateMovilRangeArray(Array_Values) {
    var RangeArray = [];

    for (var i = Array_Values.length - 1; i > 0; i--) {
        var Data1 = Math.abs(Array_Values[i]);
        var Data2 = Math.abs(Array_Values[i - 1]);
        var Cal = Math.abs(Data1 - Data2);

        RangeArray.push(Number(Cal.toFixed(2)));
    }
    var Data1 = Math.abs(Array_Values[0]);
    var Data2 = Math.abs(Array_Values[Array_Values.length - 1]);
    var Cal = Math.abs(Data1 - Data2);
    RangeArray.push(Number(Cal.toFixed(2)));

    return RangeArray.reverse();
}

function CalculateSumatoria(Array_Values) {

    var Sumatoria = 0;

    for (var i = 0; i < Array_Values.length; i++) {
        Sumatoria += Number(Array_Values[i]);
    }
    return Sumatoria;
}

function CalculateMedia(Array_Values) {

    var Sumatoria = 0;

    for (var i = 0; i < Array_Values.length; i++) {
        Sumatoria += Number(Array_Values[i]);
    }
    return Sumatoria / Array_Values.length;
}

function GetGaussBelt(Array_Values, Array_Columns) {
    var Array_Values_Copy = [];



    //Array_Values=[-6,-5.4,-4.8,-4.2,-3.6,-3,-2.4,-1.8,-1.2,-0.6,0,0.6,1.2,1.8,2.4,3,3.6,4.2,4.8,5.4,6,6.6,7.2,7.8,8.4,9,9.6,10.2,10.8,11.4,12,12.6,13.2,13.8,14.4,15,15.6,16.2,16.8,17.4,18];






    for (var i = 0; i < Array_Values.length; i++) {
        Array_Values_Copy.push(Array_Values[i]);

    }

    Array_Values_Copy = Array_Values_Copy.sort(function (a, b) { return a - b });




    var Sumatoria = CalculateSumatoria(Array_Values);
    var Media = CalculateMedia(Array_Values);
    var Desviacion = CalculateDesEst(Array_Values);
    var Array_Columns_Lines = [];


    var Euler = Math.E;

    for (var i = 0; i < Array_Values_Copy.length; i++) {
        var x = Array_Values_Copy[i];

        if (Desviacion != 0) {
            var ExponenteEulerNumerador = Math.pow((x - Media), 2);
            var ExponenteEulerDenominador = 2 * (Desviacion * Desviacion);
            var ExponenteEuler = (ExponenteEulerNumerador / ExponenteEulerDenominador) * -1;

            var Denominador = Desviacion * (Math.sqrt(2 * 3.14));
            var Resultado_Euler = Math.pow(Euler, ExponenteEuler);
            var ValorResultante = (1 / Denominador) * Resultado_Euler;
            Array_Columns_Lines.push(ValorResultante.toFixed(3));
        }
        else {
            Array_Columns_Lines.push(0);
        }
    }






    return Array_Columns_Lines;
}

function AdjustCurveCuadraticRegrecion(Array_Values) {




    var XValues = [];
    var AdjustedCurve = [];
    var XCuadraticValue = [];
    var XCubeValue = [];
    var X4tValue = [];
    var XporY = [];
    var XCuadraticPorYValue = [];





    for (var i = 0; i < Array_Values.length - 1; i++) {
        XValues.push(i);
        XCuadraticValue.push(i * i);
        XCubeValue.push(i * i * i);
        X4tValue.push(i * i * i * i);
        XporY.push(Array_Values[i] * i);
        XCuadraticPorYValue.push((i * i) * Array_Values[i]);
    }



    //Sumatorias
    var N = Array_Values.length;
    var SumatoriaX = CalculateSumatoria(XValues);
    var SumatoriaY = CalculateSumatoria(Array_Values);
    var SumatoriaXCuadratic = CalculateSumatoria(XCuadraticValue);
    var SumatoriaXCube = CalculateSumatoria(XCubeValue);
    var SumatoriaX4t = CalculateSumatoria(X4tValue);
    var SumatoriaXporY = CalculateSumatoria(XporY);
    var SumatoriaXCuadraticPorY = CalculateSumatoria(XCuadraticPorYValue);

    var Ecuacion1 = "x*" + N + "+y*" + SumatoriaX + "+z*" + SumatoriaXCuadratic + "=" + SumatoriaY;


    //var BSub0 = nerdamer.solve('x*7+y*15+z*55=94.36043000000001', 'x');

    //Despejando X en ecuacion 1
    var BSub0 = nerdamer.solve(Ecuacion1.toString(), 'x');



    //Evaluando BSub0 en ecuacion 2
    var Ecuacion2 = SumatoriaX + "*(" + BSub0.toString() + ")" + "+y*" + SumatoriaXCuadratic + "+z*" + SumatoriaXCube + "=" + SumatoriaXporY;
    var Ecuacion3 = BSub0.toString() + "*" + SumatoriaXCuadratic + "+y*" + SumatoriaXCube + "+" + "z*" + SumatoriaX4t + "=" + SumatoriaXCuadraticPorY;


    Ecuacion2 = Ecuacion2.toString().replace(']', ')');
    Ecuacion2 = Ecuacion2.toString().replace('[', '(');
    Ecuacion3 = Ecuacion3.toString().replace(']', ')');
    Ecuacion3 = Ecuacion3.toString().replace('[', '(');
    //Despejando Bsub1 
    var Ecuacion4 = nerdamer.solve(Ecuacion2.toString(), 'z');


    Ecuacion4 = Ecuacion4.toString().replace(']', ')');
    Ecuacion4 = Ecuacion4.toString().replace('[', '(');


    var Ecuacion5 = nerdamer.solve(Ecuacion3.toString(), 'z');
    Ecuacion5 = Ecuacion5.toString().replace(']', ')');
    Ecuacion5 = Ecuacion5.toString().replace('[', '(');
    //Interceptando ecuacioness

    var Ecuacion6 = Ecuacion4.toString() + "=" + Ecuacion5.toString();
    //Sustituyendo Bsub1 en ecuacion 3


    var Valor_Y = nerdamer.solve(Ecuacion6.toString(), 'y');
    Valor_Y = Valor_Y.toString().replace(']', ')');
    Valor_Y = Valor_Y.toString().replace('[', '(');

    Valor_Y = eval(Valor_Y);
    //Sustituyendo Y en 3 para obtener valor de z

    BSub0 = BSub0.toString().replace('y', Valor_Y);
    BSub0 = BSub0.toString().replace(']', ')');
    BSub0 = BSub0.toString().replace('[', '(');

    Ecuacion3 = BSub0.toString() + "*" + SumatoriaXCuadratic + "+" + Valor_Y.toString() + "*" + SumatoriaXCube + "+" + "z*" + SumatoriaX4t + "=" + SumatoriaXCuadraticPorY;

    Ecuacion3 = Ecuacion3.toString().replace(']', ')');
    Ecuacion3 = Ecuacion3.toString().replace('[', '(');

    var Valor_Z = nerdamer.solve(Ecuacion3.toString(), 'z');
    Valor_Z = Valor_Z.toString().replace(']', ')');
    Valor_Z = Valor_Z.toString().replace('[', '(');
    Valor_Z = eval(Valor_Z);
    Ecuacion1 = "x*" + N + "+" + Valor_Y.toString() + "*" + SumatoriaX + "+" + Valor_Z.toString() + "*" + SumatoriaXCuadratic + "=" + SumatoriaY;

    var Valor_X = nerdamer.solve(Ecuacion1.toString(), 'x');
    Valor_X = Valor_X.toString().replace(']', ')');
    Valor_X = Valor_X.toString().replace('[', '(');
    Valor_X = eval(Valor_X);
    ///Con los valores de X,Y,Z tengo la ecuacion de la parabola que es:



    var AdjustedCurveValues = [];

    for (var i = 0; i < Array_Values.length; i++) {

        var Factor1 = Valor_X;
        var Factor2 = Number(Valor_Y) * i;
        var Factor3 = Number(Valor_Z) * (i * i);

        var Temp = Factor1 + Factor2 + Factor3;


        AdjustedCurve.push(Temp.toFixed(3));
    }



    var AdjustedCurveValuesFinal = [];
    var MaxValue = 0;



    for (var i = 0; i < AdjustedCurve.length; i++) {
        var ValorTemporal = AdjustedCurve[i];
        if (Number(ValorTemporal) >= Number(MaxValue) && (i < 15)) {
            MaxValue = ValorTemporal;
            AdjustedCurveValuesFinal.push(ValorTemporal);
        }
        else {
            break;
        }
    }



    for (var i = AdjustedCurveValuesFinal.length - 1; i >= 0; i--) {
        AdjustedCurveValuesFinal.push(AdjustedCurveValuesFinal[i]);
    }



    return AdjustedCurveValuesFinal;
}
///////////////////////

function ProcessIRDataSincronic(spcchart, count, mean, sigma) {
    // batch number for a given sample subgroup
    var batchCounter = 0;
    var i;
    var timestamp = new Date();
    if (!spcchart) return;
    if (!spcchart.getChartData()) return;

    var currentcount = spcchart.getChartData().getCurrentNumberRecords();

    var ElementCaptures = $("#CapturetoChart").val().split("*");
    ElementCaptures = ElementCaptures.reverse();

    for (i = 0; i < ElementCaptures.length; i++) {
        // Simulate a sample subgroup record
        var samples = spcchart.getChartData().simulateMeasurementRecordMeanRange(mean, sigma);
        samples.dataBuffer[0] = Number(ElementCaptures[i]);
        // Update chart data using i as the batch number
        batchCounter = i + 1;
        var note = "";
        if ((i % 5) == 0) note = "This is a note";
        // Add a new sample record to the chart data
        spcchart.getChartData().addNewSampleRecordBatchNumberDateSamplesNotes(batchCounter, timestamp, samples, note);

        var Temp = spcchart.getChartData();

        // Simulate passage of timeincrementminutes minutes
        QCSPCChartTS.ChartCalendar.add(timestamp, QCSPCChartTS.ChartConstants.MINUTE, 15);
    }

}

function GetDiferenceValues(Values) {
    var FinalArray = [];

    var Final = Values[0] - Values[Values.length - 1];
    if (Final < 0)
    { Final = Final * -1; }
    FinalArray.push(Number(Final.toFixed(2)));

    for (var i = 0; i < Values.length - 1; i++) {
        var Temp = Values[i] - Values[i + 1];
        if (Temp < 0)
        { Temp = Temp * -1; }
        FinalArray.push(Number(Temp.toFixed(2)));
    }


    return FinalArray;
}

function GetValuesForMultipleArray(CurrentObject, ArrayValues) {
    var CurrentDate = CurrentObject.Fecha;
    var Moved = 0;
    var SumPartial = 0;
    for (var j = 0; j < ArrayValues.length; j++) {
        if (ArrayValues[j].Fecha == CurrentDate)
        {
            SumPartial += Number(ArrayValues[j].Valor);
            Moved++;
        }
    }

    return SumPartial / Moved;
}

function EvaluateCurrentObject(CurrentObject, ArrayValuesAgrupathed) {
    var Result = false;
    var CurrentDate = CurrentObject.Fecha;
    for (var i = 0; i < ArrayValuesAgrupathed.length; i++) {
        if (ArrayValuesAgrupathed[i].Fecha == CurrentDate)
        {
            Result = true;
            break;
        }
    }
    return Result;
}

///////////////////////
