﻿

//function LoadDataTableTasks() {

//	$('#DatatableTasks').DataTable({
//        "ajax": {
//        	"url": "/Tasks/GetTasks",
//            "type": "GET",
//            "datatype": "Json"
//        },
//        "language": {
//            "url": "../Content/Languaje/Data_table_ES.json"
//        },

//        "columnDefs": [
//            { "className": 'dt-center', "targets": "_all" }
//        ],

//        "columns": [
//            { "data": "name", "title": "Nombre", "autoWidth": false, },
//            { "data": "description", "title": "Descripción", "autoWidth": false },
//            {
//                "data": "Id", "autoWidth": false, "render": function (data) {
//                    return "<a class='btn btn-default btn-sm' id='Edit_Linea' data-id=" + data + " title='Editar'><i class='fa fa-pencil'></i></a> <a class='btn btn-danger btn-sm' id='Delete_Linea' data-id=" + data + " title='Borrar'><i class='fa fa-trash'></i></a>"
//                }
//            }
//        ]
//    });
//}

function LoadDataTableEmpresas() {

    $('#DatatableEmpresas').DataTable({
        "ajax": {
            "url": "/Empresas/GetEmpresas",
            "type": "GET",
            "datatype": "Json"
        },
        "language": {
            "url": "../Content/Languaje/Data_table_ES.json"
        },

        "columnDefs": [
            { "className": 'dt-center', "targets": "_all" }
        ],
        "columns": [
            { "data": "RazonSocial", "title": "Empresa", "autoWidth": true, },
            { "data": "Administrador", "title": "Administrador", "autoWidth": true },
            { "data": "Telefono", "title": "Teléfono", "autoWidth": true },
            { "data": "Email", "title": "Emails", "autoWidth": true },
            { "data": "Contacto1", "title": "Contactos", "autoWidth": true },
            { "data": "Fecha_Vencimiento", "title": "Vencimiento", "autoWidth": true },
            {
                "data": "Id", "autoWidth": true, "render": function (data) {
                    return "<div class='row'><a class='btn btn-default btn-sm' id='Edit_Empresas' data-id=" + data + " ><i class='fa fa-pencil'></i></a> <a class='btn btn-danger btn-sm' id='Delete_Empresas' data-id=" + data + "><i class='fa fa-trash'></i></a></div>"
                }
            }
        ]
    });
}

function LoadDatatableEquipos() {

    $('#DatatableEquipos').DataTable({
        "ajax": {
            "url": "/Equipos/GetEquipos",
            "type": "GET",
            "datatype": "Json"
        },
        "language": {
            "url": "../Content/Languaje/Data_table_ES.json"
        },

        "columnDefs": [
            { "className": 'dt-center', "targets": "_all" }
        ],

        "columns": [
            { "data": "NombreEquipo", "title": "Nombre del Equipo", "autoWidth": false, },
            {
                "data": "Id", "autoWidth": false, "render": function (data) {
                    return "<a class='btn btn-default btn-sm' id='Edit_Equipo' data-id=" + data + " title='Editar'><i class='fa fa-pencil'></i></a> <a class='btn btn-danger btn-sm' id='Delete_Equipo' data-id=" + data + " title='Borrar'><i class='fa fa-trash'></i></a>"
                }
            }
        ]
    });
}

function LoadDatatableEquiposReport() {

    $('#DatatableEquiposReport').DataTable({
        "ajax": {
            "url": "/Equipos/GetEquiposReport",
            "type": "GET",
            "datatype": "Json"
        },
        "language": {
            "url": "../Content/Languaje/Data_table_ES.json"
        },

        "columnDefs": [
            { "className": 'dt-center', "targets": "_all" }
        ],

        "columns": [
             {
                 "data": "Id", "autoWidth": false, "render": function (data) {
                     return "<a class='btn btn-default btn-sm' id='Mark_Equipo' title='Seleccionar'><i  id=" + data + " class='fa fa-square-o'></i></a></i></a>"
                 }, defaultContent: ''
             },
            { "data": "NombreEquipo", "title": "Nombre del Equipo", "autoWidth": false, defaultContent: '' }
           
        ]
    });
}

function LoadDataTableRoles() {

    $('#DatatableRoles').DataTable({
        "ajax": {
            "url": "/AspNetRoles/GetRoles",
            "type": "GET",
            "datatype": "Json"
        },
        "language": {
            "url": "../Content/Languaje/Data_table_ES.json"
        },

        "columnDefs": [
            { "className": 'dt-center', "targets": "_all" }
        ],

        "columns": [
            { "data": "Name", "title": "Rol", "autoWidth": false, },
            //{ "data": "Manipular_Empresas", "title": "Menipular Empresas", "autoWidth": false },
                {
                    "data": "Manipular_Empresas", "title": "Manipular Empresas", "autoWidth": false, "className": "text-center", "targets": "_all", "render": function (data) {
                        if (data == true) {
                            return "<i class='glyphicon glyphicon-ok-circle' style='color:green'></i>"
                        }
                        else {
                            return "<i class='glyphicon glyphicon-ban-circle' style='color:red'></i>"
                        }
                    }
                },
            //{ "data": "Crear_Usuarios", "title": "Crear Usuarios", "autoWidth": false },
             {
                 "data": "Crear_Usuarios", "title": "Crear Usuarios", "autoWidth": false, "className": "text-center", "targets": "_all", "render": function (data) {
                     if (data == true) {
                         return "<i class='glyphicon glyphicon-ok-circle' style='color:green'></i>"
                     }
                     else {
                         return "<i class='glyphicon glyphicon-ban-circle' style='color:red'></i>"
                     }
                 }
             },
            //{ "data": "Editar_Usuarios", "title": "Editar Usuarios", "autoWidth": false },
             {
                 "data": "Editar_Usuarios", "title": "Editar Usuarios", "autoWidth": false, "className": "text-center", "targets": "_all", "render": function (data) {
                     if (data == true) {
                         return "<i class='glyphicon glyphicon-ok-circle' style='color:green'></i>"
                     }
                     else {
                         return "<i class='glyphicon glyphicon-ban-circle' style='color:red'></i>"
                     }
                 }
             },
            //{ "data": "Eliminar_Usuarios", "title": "Eliminar Usuarios", "autoWidth": false },
             {
                 "data": "Eliminar_Usuarios", "title": "Eliminar Usuarios", "autoWidth": false, "className": "text-center", "targets": "_all", "render": function (data) {
                     if (data == true) {
                         return "<i class='glyphicon glyphicon-ok-circle' style='color:green'></i>"
                     }
                     else {
                         return "<i class='glyphicon glyphicon-ban-circle' style='color:red'></i>"
                     }
                 }
             },
            {
                "data": "Id", "autoWidth": false, "render": function (data) {
                    return "<a class='btn btn-default btn-sm' id='Edit_Rol' data-id=" + data + " title='Editar'><i class='fa fa-pencil'></i></a> <a class='btn btn-danger btn-sm' id='Delete_Rol' data-id=" + data + " title='Borrar'><i class='fa fa-trash'></i></a>"
                }
            }
        ]
    });
}
function LoadDataTableImporterUser() {

    $('#DatatableImporterUser').DataTable({
        "ajax": {
            "url": "/Usuarios/GetImportUsers",
            "type": "POST",
            "datatype": "Json"
        },
        "language": {
            "url": "/Content/Languaje/Data_table_ES.json"
        },

        "columnDefs": [
            { "className": 'dt-center', "targets": "_all" }
        ],

        "columns": [
            { "data": "Nombre", "title": "Nombre", "autoWidth": true, },
            { "data": "Apellidos", "title": "Apellidos", "autoWidth": true },
            { "data": "UserName", "title": "Telefono", "autoWidth": true },
            { "data": "Email", "title": "Email", "autoWidth": true }


        ]
    });
}
function LoadDataTableFormas() {

    $('#DatatableFormas').DataTable({
        "ajax": {
            "url": "/Formas/GetFormas",
            "type": "GET",
            "datatype": "Json"
        },
        "language": {
            "url": "../Content/Languaje/Data_table_ES.json"
        },

        "columnDefs": [
            { "className": 'dt-center', "targets": "_all" }
        ],

        "columns": [
            { "data": "NombreForma", "title": "Nombre de la Forma", "autoWidth": false, },
            {
                "data": "Id", "autoWidth": false, "render": function (data) {
                    return "<a class='btn btn-default btn-sm' id='Edit_Forma' data-id=" + data + " title='Editar'><i class='fa fa-pencil'></i></a>"
                }
            }
        ]
    });
}

function LoadDataTableFormsAsigned() {

    $('#DatatableFormas').DataTable({
        "ajax": {
            "url": "/Auditoria/GetFormsAsigned",
            "type": "GET",
            "datatype": "Json"
        },
        "language": {
            "url": "../Content/Languaje/Data_table_ES.json"
        },

        "columnDefs": [
            { "className": 'dt-center', "targets": "_all" }
        ],

        "columns": [
            { "data": "Nombre_Audit", "title": "Nombre de la Forma", "autoWidth": false, },
            { "data": "Fecha_Inicio", "title": "Fecha de Inicio", "autoWidth": false, },
            { "data": "Fecha_Fin", "title": "Fecha de Fin", "autoWidth": false, },
            { "data": "Perioricidad", "title": "Perioricidad", "autoWidth": false, },
            {
                "data": "Id", "autoWidth": false, "render": function (data) {
                    return "<a class='btn btn-default btn-sm' id='Edit_Forma' data-id=" + data + " title='Editar'><i class='fa fa-pencil'></i></a> <a class='btn btn-danger btn-sm' id='Delete_Forma' data-id=" + data + " title='Borrar'><i class='fa fa-trash'></i></a>"
                }
            }
        ]
    });
}

function LoadDataTableAnnouncement() {

    
}

function UpdateDataTableLineas() {
    $('#DatatableLineas').DataTable().ajax.reload();
}

function MailValidate(Cntc_Email, Name) {

    var result = true;
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!expr.test(Cntc_Email)) {
        $("#" + Name).parent().append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este valor debe ser un correo válido</span>');
        result = false;
    }
    return result;
}

$(".btnChexbox").click(function (eve) {

    if ($(this).hasClass("glyphicon-unchecked")) {
        $(this).removeClass("glyphicon-unchecked");
        $(this).addClass("glyphicon-check");
    }
    else {
        $(this).removeClass("glyphicon-check");
        $(this).addClass("glyphicon-unchecked");
    }
})
