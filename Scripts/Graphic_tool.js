﻿function Create_Pie_Graph(id_component,realizado,no_realizado) {

    var ctx = document.getElementById(id_component).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: [realizado + "% Realizado",no_realizado + "% No Realizado"],
            datasets: [{
                label: '# of Votes',
                data: [realizado, no_realizado],
                backgroundColor: [

                    'rgba(89, 192, 192, 1)',

                    'rgba(255, 159, 64, 1)'
                ],
                borderColor: [

                    'rgba(75, 192, 192, 1)',
      
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
              
            }
        }
    });
}

function Create_Bar_Graph(id_component, por_debajo, en_rango, por_arriba) {

    var ctx = document.getElementById(id_component).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
       
        data: {
            labels: ["% Inferior", "% En Rango", "% Por Encima"],
            datasets: [{
                label: '',
                data: [por_debajo, en_rango, por_arriba],
                backgroundColor: [
                  
                    'rgba(89, 192, 192, 1)',
                         'rgba(33, 35, 229, 0.9)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderColor: [

                    'rgba(75, 192, 192, 1)',
                           'rgba(04, 04, B4, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false,
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function Create_Doughnut_Graph(id_component, correctas, incorrectas) {

    var ctx = document.getElementById(id_component).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',

        data: {
            labels: ["% Correctas", "% Incorrectas"],
            datasets: [{
                label: '',
                data: [correctas, incorrectas],
                backgroundColor: [

                    'rgba(89, 192, 192, 1)',
                         'rgba(33, 35, 229, 0.9)',
                
                ],
                borderColor: [

                    'rgba(75, 192, 192, 1)',
                           'rgba(04, 04, B4, 1)',
  
                ],
                borderWidth: 1
            }]
        },
        options: {
  
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}


function Create_Line_Time(id_component, valores, momento) {

    //data: [{
    //    x: 10,
    //    y: 20
    //}, {
    //    x: 15,
    //    y: 10
    //}]

    new Chart(document.getElementById("id_component"), {
        type: 'line',
        data: {
            labels: [1500, 1600, 1700, 1750, 1800, 1850, 1900, 1950, 1999, 2050],
            datasets: [{
                data: [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478],
                label: "Africa",
                borderColor: "#3e95cd",
                fill: false
            }, {
                data: [282, 350, 411, 502, 635, 809, 947, 1402, 3700, 5267],
                label: "Asia",
                borderColor: "#8e5ea2",
                fill: false
            }, {
                data: [168, 170, 178, 190, 203, 276, 408, 547, 675, 734],
                label: "Europe",
                borderColor: "#3cba9f",
                fill: false
            }, {
                data: [40, 20, 10, 16, 24, 38, 74, 167, 508, 784],
                label: "Latin America",
                borderColor: "#e8c3b9",
                fill: false
            }, {
                data: [6, 3, 2, 2, 7, 26, 82, 172, 312, 433],
                label: "North America",
                borderColor: "#c45850",
                fill: false
            }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'World population per region (in millions)'
            }
        }
    });
}
