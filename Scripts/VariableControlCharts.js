﻿import * as  QCSPCChartTS from './../lib/QCSPCChartTS/qcspcchartts.js';

var Ewma8_2Data = 
[9.45, 7.99, 9.29, 11.66, 12.16, 10.18, 8.04, 11.46, 9.2, 10.34, 9.03, 11.47, 10.51, 9.4, 10.08, 9.37, 10.62, 10.31, 8.52, 10.84, 10.9, 9.33, 12.29, 11.5, 10.6, 11.08, 10.38, 11.62, 11.31, 10.52];

var xBarRChartObj = null;
var xBarRChartCanvas = null;

function alarmEventChanged( source,  e)
{

   var alarm = e.getEventAlarm();
   if (alarm)
   {
     var alarmlimitvalue = alarm.getControlLimitValue();
     var alarmlimitvaluestring =  alarmlimitvalue.toString();
     var spcData  = alarm.getSPCProcessVar();
   }
}

export async function AddDataXBarRChart( mean, stddev, count) {

    if (xBarRChartObj) {
        SimulateData(xBarRChartObj, count, mean, stddev);
        // Scale the y-axis of the X-Bar chart to display all data and control limits
        xBarRChartObj.autoScalePrimaryChartYRange();
        // Scale the y-axis of the Range chart to display all data and control limits
        xBarRChartObj.autoScaleSecondaryChartYRange();
        // Rebuild the chart using the current data and settings
        xBarRChartObj.rebuildChartUsingCurrentData();
    }
}

export async function BuildXBarRChart(canvasid) {

    var htmlcanvas = xBarRChartCanvas = document.getElementById(canvasid);
    var spccharttype = QCSPCChartTS.SPCControlChartData.MEAN_RANGE_CHART; 
    var subgroupsize = 5;
    var numberpointsinview = 12;
    var charttitle = "XBar-R Example";


    var xbarrchart = xBarRChartObj = QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchVariableControlChartChartTypeSubgroupSize(htmlcanvas, spccharttype, subgroupsize, numberpointsinview);

    xbarrchart.setPreferredSize(800, 600);

    xbarrchart.setGraphStopPosX(0.825);
    xbarrchart.setGraphStartPosX(0.18);
    xbarrchart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);
    xbarrchart.setEnableScrollBar(true);

    xbarrchart.setEnableCategoryValues(true);
    xbarrchart.setEnableCalculatedValues(true);
    xbarrchart.setEnableAlarmStatusValues(true);
    xbarrchart.setEnableChartToggles(true);
    xbarrchart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    xbarrchart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);

    var chartdata = xbarrchart.getChartData();
    if (chartdata)
    {
        chartdata.setTitle (charttitle);
        chartdata.setChartDescriptor("XBar-R");
        chartdata.setPartNumber ( "283501");
        chartdata.setChartNumber("17");
        chartdata.setPartName( "Transmission Casing Bolt");
        chartdata.setOperation ( "Threading");
        chartdata.setOperator("J. Fenamore");
        chartdata.setMachine("#11");
         var  today = new Date();
         chartdata.setDateString(today.toLocaleString());
         chartdata.setNotesMessage( "Control limits prepared May 10");
         chartdata.setNotesHeader("NOTES"); // row header
         chartdata.addAlarmTransitionEventListener(alarmEventChanged);
         chartdata.setAlarmTransitionEventEnable(true);   
    }

    xbarrchart.setEnableDisplayOptionToggles(true);
    
    var numssampleintervals = 100;
    var chartmean = 30;
    var chartsigma = 5;

    SimulateData(xbarrchart, numssampleintervals, chartmean, chartsigma);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    xbarrchart.autoCalculateControlLimits();

    // Out some more data, different mean and sigma, to simulate out of control
    numssampleintervals = 50;
    chartmean = 32;
    chartsigma = 8;

    SimulateData(xbarrchart, numssampleintervals, chartmean, chartsigma);

    // Scale the y-axis of the X-Bar chart to display all data and control limits
    xbarrchart.autoScalePrimaryChartYRange();
    // Scale the y-axis of the Range chart to display all data and control limits
    xbarrchart.autoScaleSecondaryChartYRange();
    // Rebuild the chart using the current data and settings
    xbarrchart.rebuildChartUsingCurrentData();

/**
    var sigma2 = 2.0;
    var sigma1 = 1.0;

    var primarychart = xbarrchart.getPrimaryChart(); 
    var secondarychart = xbarrchart.getSecondaryChart(); 
    
    if (!primarychart) return;
    if (!secondarychart) return;
    if (!chartdata) return;

    var lcllimit = primarychart.getControlLimitDataIndex(QCSPCChartTS.SPCChartObjects.SPC_LOWER_CONTROL_LIMIT);
    if (lcllimit)  lcllimit.setLimitValue(74);

    var ucllimit = primarychart.getControlLimitDataIndex(QCSPCChartTS.SPCChartObjects.SPC_UPPER_CONTROL_LIMIT);
    if (ucllimit) ucllimit.setLimitValue(76);

    var target = primarychart.getControlLimitDataIndex(QCSPCChartTS.SPCChartObjects.SPC_CONTROL_TARGET);
    if (target) target.setLimitValue(75);
    
    // Create multiple limits
    // For PrimaryChart
    var lcl2 = QCSPCChartTS.SPCControlLimitRecord.newSPCControlLimitRecordProcessVarAlarmTypeAlarmValueAlarmMessages(chartdata, QCSPCChartTS.SPCControlLimitRecord.SPC_LOWERTHAN_LIMIT, 74.33,"LCL2", "LCL2");
    var ucl2 = QCSPCChartTS.SPCControlLimitRecord.newSPCControlLimitRecordProcessVarAlarmTypeAlarmValueAlarmMessages(chartdata, QCSPCChartTS.SPCControlLimitRecord.SPC_GREATERTHAN_LIMIT, 75.66,"UCL2", "UCL2");
    
    primarychart.addAdditionalControlLimit(lcl2, QCSPCChartTS.SPCChartObjects.SPC_LOWER_CONTROL_LIMIT_2, sigma2);
    primarychart.addAdditionalControlLimit(ucl2, QCSPCChartTS.SPCChartObjects.SPC_UPPER_CONTROL_LIMIT_2, sigma2);
    var lcl3 = QCSPCChartTS.SPCControlLimitRecord.newSPCControlLimitRecordProcessVarAlarmTypeAlarmValueAlarmMessages(chartdata, QCSPCChartTS.SPCControlLimitRecord.SPC_LOWERTHAN_LIMIT, 74.66,"LCL1", "LCL1");
    var ucl3 = QCSPCChartTS.SPCControlLimitRecord.newSPCControlLimitRecordProcessVarAlarmTypeAlarmValueAlarmMessages(chartdata, QCSPCChartTS.SPCControlLimitRecord.SPC_GREATERTHAN_LIMIT, 75.33,"UCL1", "UCL1");
    
    primarychart.addAdditionalControlLimit(lcl3, QCSPCChartTS.SPCChartObjects.SPC_LOWER_CONTROL_LIMIT_1, sigma1);
    primarychart.addAdditionalControlLimit(ucl3, QCSPCChartTS.SPCChartObjects.SPC_UPPER_CONTROL_LIMIT_1, sigma1);
    

    lcllimit = secondarychart.getControlLimitDataIndex(QCSPCChartTS.SPCChartObjects.SPC_LOWER_CONTROL_LIMIT);
    if (lcllimit)  lcllimit.setLimitValue(2);

    ucllimit = secondarychart.getControlLimitDataIndex(QCSPCChartTS.SPCChartObjects.SPC_UPPER_CONTROL_LIMIT);
    if (ucllimit) ucllimit.setLimitValue(0);

    target = secondarychart.getControlLimitDataIndex(QCSPCChartTS.SPCChartObjects.SPC_CONTROL_TARGET);
    if (target) target.setLimitValue(1);

    
    var	lcl4 = QCSPCChartTS.SPCControlLimitRecord.newSPCControlLimitRecordProcessVarAlarmTypeAlarmValueAlarmMessages(chartdata, QCSPCChartTS.SPCControlLimitRecord.SPC_LOWERTHAN_LIMIT, 0.33,"LCL2", "LCL2");
    var	ucl4 = QCSPCChartTS.SPCControlLimitRecord.newSPCControlLimitRecordProcessVarAlarmTypeAlarmValueAlarmMessages(chartdata, QCSPCChartTS.SPCControlLimitRecord.SPC_GREATERTHAN_LIMIT, 1.66,"UCL2", "UCL2");
    
    secondarychart.addAdditionalControlLimit(lcl4, QCSPCChartTS.SPCChartObjects.SPC_LOWER_CONTROL_LIMIT_2, sigma2);
    secondarychart.addAdditionalControlLimit(ucl4, QCSPCChartTS.SPCChartObjects.SPC_UPPER_CONTROL_LIMIT_2, sigma2);
    
    var	lcl5 = QCSPCChartTS.SPCControlLimitRecord.newSPCControlLimitRecordProcessVarAlarmTypeAlarmValueAlarmMessages(chartdata, QCSPCChartTS.SPCControlLimitRecord.SPC_LOWERTHAN_LIMIT, 0.66,"LCL1", "LCL1");
    var	ucl5 = QCSPCChartTS.SPCControlLimitRecord.newSPCControlLimitRecordProcessVarAlarmTypeAlarmValueAlarmMessages(chartdata, QCSPCChartTS.SPCControlLimitRecord.SPC_GREATERTHAN_LIMIT, 1.33,"UCL1", "UCL1");
    
    secondarychart.addAdditionalControlLimit(lcl5, QCSPCChartTS.SPCChartObjects.SPC_LOWER_CONTROL_LIMIT_1, sigma1);
    secondarychart.addAdditionalControlLimit(ucl5, QCSPCChartTS.SPCChartObjects.SPC_UPPER_CONTROL_LIMIT_1, sigma1);
    
    primarychart.setControlLimitLineFillMode( true);
    
    secondarychart.setControlLimitLineFillMode( true);    
*/    
}

export async function BuildXBarSigmaChart(canvasid) {


    var htmlcanvas = document.getElementById(canvasid);
    var spccharttype = QCSPCChartTS.SPCControlChartData.MEAN_SIGMA_CHART; 
    var subgroupsize = 5;
    var numberpointsinview = 12;
    var charttitle = "XBar-Sigma Example";

    var xbarsigmachart = QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchVariableControlChartChartTypeSubgroupSize(htmlcanvas, spccharttype, subgroupsize, numberpointsinview);

    xbarsigmachart.setPreferredSize(800, 600);


    xbarsigmachart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);

    xbarsigmachart.setEnableScrollBar(true);
    xbarsigmachart.setEnableDisplayOptionToggles(true);
    xbarsigmachart.setEnableCategoryValues(true);
    xbarsigmachart.setEnableCalculatedValues(true);
    xbarsigmachart.setEnableAlarmStatusValues(true);
    xbarsigmachart.setEnableChartToggles(true);
    xbarsigmachart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    xbarsigmachart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);
    var chartdata = xbarsigmachart.getChartData();
    if (chartdata) {
    chartdata.setTitle (charttitle);
    chartdata.setChartDescriptor("XBar-Sigma");
    chartdata.setProcessCapabilityLSLValue(27);
    chartdata.setProcessCapabilityUSLValue(35);
    chartdata.addProcessCapabilityValue(QCSPCChartTS.SPCProcessCapabilityRecord.SPC_CPK_CALC);     
    chartdata.addProcessCapabilityValue(QCSPCChartTS.SPCProcessCapabilityRecord.SPC_CPM_CALC);     
    chartdata.addProcessCapabilityValue(QCSPCChartTS.SPCProcessCapabilityRecord.SPC_PPK_CALC);    
    }

    var charttable = xbarsigmachart.getChartTable();

    var primarychart = xbarsigmachart.getPrimaryChart(); 
    var secondarychart = xbarsigmachart.getSecondaryChart(); 
    if (primarychart)
    {
// Set symbol emphasis type, and size, for primary chart
      primarychart.setOutOfLimitSymbolNumber( QCSPCChartTS.ChartConstants.PLUS);
      primarychart.setOutOfLimitSymbolSize(16);

     // used in setDisplayFrequencyHistogram example
    //  primarychart.setDisplayFrequencyHistogram( false);

    // Used in a spec limit example
  //  var lowspecattrib = QCSPCChartTS.ChartAttribute.newChartAttribute3(QCSPCChartTS.ChartColor.GREEN, 3, QCSPCChartTS.ChartConstants.LS_DASH_8_4);
  //  var upperspecattrib = QCSPCChartTS.ChartAttribute.newChartAttribute3(QCSPCChartTS.ChartColor.ORANGE, 3, QCSPCChartTS.ChartConstants.LS_DASH_8_4);
  //  primarychart.addSpecLimit(QCSPCChartTS.SPCChartObjects.SPC_LOWER_SPEC_LIMIT, 18.3, "L SPEC", lowspecattrib);
  //  primarychart.addSpecLimit(QCSPCChartTS.SPCChartObjects.SPC_UPPER_SPEC_LIMIT, 39.1, "H SPEC", upperspecattrib);      
    }
    if (secondarychart)
    {
// Set symbol emphasis type, and size, for secondary chart
      secondarychart.setOutOfLimitSymbolNumber( QCSPCChartTS.ChartConstants.SQUARE);
      secondarychart.setOutOfLimitSymbolSize(14);
    }
    if (charttable)
    {
        charttable.setTableBackgroundMode(QCSPCChartTS.SPCGeneralizedTableDisplay. TABLE_NO_COLOR_BACKGROUND );
    }
    var numssampleintervals = 100;
    var chartmean = 30;
    var chartsigma = 5;

    SimulateData(xbarsigmachart, numssampleintervals, chartmean, chartsigma);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    xbarsigmachart.autoCalculateControlLimits();

        // Out some more data, different mean and sigma, to simulate out of control
        numssampleintervals = 50;
        chartmean = 32;
        chartsigma = 8;
        
    SimulateData(xbarsigmachart, numssampleintervals, chartmean, chartsigma);

    // Scale the y-axis of the X-Bar chart to display all data and control limits
    xbarsigmachart.autoScalePrimaryChartYRange();
    // Scale the y-axis of the Range chart to display all data and control limits
    xbarsigmachart.autoScaleSecondaryChartYRange();
    // Rebuild the chart using the current data and settings
    xbarsigmachart.rebuildChartUsingCurrentData();


}

export async function BuildMedianRangeChart(canvasid) {


    var htmlcanvas = document.getElementById(canvasid);
    var spccharttype = QCSPCChartTS.SPCControlChartData.MEDIAN_RANGE_CHART; 
    var subgroupsize = 5;
    var numberpointsinview = 12;
    var charttitle = "Median-Range Example";

     var medianrangechart = QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchVariableControlChartChartTypeSubgroupSize(htmlcanvas, spccharttype, subgroupsize, numberpointsinview);

    medianrangechart.setPreferredSize(800, 600);


    medianrangechart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);

    medianrangechart.setEnableScrollBar(true);
    medianrangechart.setEnableDisplayOptionToggles(true);
    medianrangechart.setEnableCategoryValues(true);
    medianrangechart.setEnableCalculatedValues(true);
    medianrangechart.setEnableAlarmStatusValues(true);
    medianrangechart.setEnableChartToggles(true);
    medianrangechart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    medianrangechart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);
    medianrangechart.getChartData().setTitle (charttitle);
    medianrangechart.getChartData().setChartDescriptor("Median-Range");
    var charttable = medianrangechart.getChartTable();

    if (charttable)
    {
        charttable.setTableBackgroundMode(   QCSPCChartTS.SPCGeneralizedTableDisplay.TABLE_SINGLE_COLOR_BACKGROUND );
        charttable.setBackgroundColor1( QCSPCChartTS.ChartColor.LIGHTGRAY);
    }
    var numssampleintervals = 100;
    var chartmean = 30;
    var chartsigma = 5;

    SimulateData(medianrangechart, numssampleintervals, chartmean, chartsigma);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    medianrangechart.autoCalculateControlLimits();

        // Out some more data, different mean and sigma, to simulate out of control
        numssampleintervals = 50;
        chartmean = 32;
        chartsigma = 8;
        
    SimulateData(medianrangechart, numssampleintervals, chartmean, chartsigma);

    // Scale the y-axis of the X-Bar chart to display all data and control limits
    medianrangechart.autoScalePrimaryChartYRange();
    // Scale the y-axis of the Range chart to display all data and control limits
    medianrangechart.autoScaleSecondaryChartYRange();
    // Rebuild the chart using the current data and settings
    medianrangechart.rebuildChartUsingCurrentData();


}

export async function BuildIRChart(canvasid) {


    var htmlcanvas = document.getElementById(canvasid);
    var spccharttype = QCSPCChartTS.SPCControlChartData.INDIVIDUAL_RANGE_CHART;
    var subgroupsize = 1;
    var numberpointsinview = 12;
    var charttitle = "I-R Example";

    var irchart = QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchVariableControlChartChartTypeSubgroupSize(htmlcanvas, spccharttype, subgroupsize, numberpointsinview);

    irchart.setPreferredSize(800, 600);

 
    irchart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);

    irchart.setEnableScrollBar(true);
    irchart.setEnableDisplayOptionToggles(true);
    irchart.setEnableCategoryValues(true);
    irchart.setEnableCalculatedValues(true);
    irchart.setEnableAlarmStatusValues(true);
    irchart.setEnableChartToggles(true);
    irchart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    irchart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);
    var chartdata = irchart.getChartData();
    if (chartdata) {
      chartdata.setTitle (charttitle);
      chartdata.setChartDescriptor("I-R");
    }

    var charttable = irchart.getChartTable();

    if (charttable)
    {
        charttable.setTableBackgroundMode(   QCSPCChartTS.SPCGeneralizedTableDisplay.TABLE_STRIPED_COLOR_BACKGROUND);
        charttable.setBackgroundColor1( QCSPCChartTS.ChartColor.BISQUE);
        charttable.setBackgroundColor2( QCSPCChartTS.ChartColor.LIGHTGOLDENRODYELLOW);
    }
    var numssampleintervals = 100;
    var chartmean = 30;
    var chartsigma = 5;

    SimulateData(irchart, numssampleintervals, chartmean, chartsigma);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    irchart.autoCalculateControlLimits();

            // Out some more data, different mean and sigma, to simulate out of control
            numssampleintervals = 50;
            chartmean = 32;
            chartsigma = 8;
            
        SimulateData(irchart, numssampleintervals, chartmean, chartsigma);

    // Scale the y-axis of the X-Bar chart to display all data and control limits
    irchart.autoScalePrimaryChartYRange();
    // Scale the y-axis of the Range chart to display all data and control limits
    irchart.autoScaleSecondaryChartYRange();
    // Rebuild the chart using the current data and settings
    irchart.rebuildChartUsingCurrentData();


}


export async function BuildLeveyJenningsChart(canvasid) {


    var htmlcanvas = document.getElementById(canvasid);
    var spccharttype = QCSPCChartTS.SPCControlChartData.LEVEY_JENNINGS_CHART; 
    var subgroupsize = 1;
    var numberpointsinview = 12;
    var charttitle = "Levey-Jennings Example";

      var leveyjenningschart = QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchVariableControlChartChartTypeSubgroupSize(htmlcanvas, spccharttype, subgroupsize, numberpointsinview);

    leveyjenningschart.setPreferredSize(800, 600);

    leveyjenningschart.setGraphStopPosX(0.825);
    leveyjenningschart.setGraphStartPosX(0.18);
    leveyjenningschart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);
    leveyjenningschart.setEnableScrollBar(true);
    leveyjenningschart.setEnableDisplayOptionToggles(true);
    leveyjenningschart.setEnableCategoryValues(true);
    leveyjenningschart.setEnableCalculatedValues(true);
    leveyjenningschart.setEnableAlarmStatusValues(true);
    leveyjenningschart.setEnableChartToggles(true);
    leveyjenningschart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    leveyjenningschart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);

    leveyjenningschart.getChartData().setTitle (charttitle);
    leveyjenningschart.getChartData().setChartDescriptor("Levey-Jennings");
    var charttable = leveyjenningschart.getChartTable();

    if (charttable)
    {
        charttable.setTableBackgroundMode(   QCSPCChartTS.SPCGeneralizedTableDisplay.TABLE_SINGLE_COLOR_BACKGROUND );
        charttable.setBackgroundColor1( QCSPCChartTS.ChartColor.LIGHTGRAY);
    }

    var numssampleintervals = 100;
    var chartmean = 30;
    var chartsigma = 5;

    SimulateData(leveyjenningschart, numssampleintervals, chartmean, chartsigma);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    leveyjenningschart.autoCalculateControlLimits();

        // Out some more data, different mean and sigma, to simulate out of control
        numssampleintervals = 50;
        chartmean = 32;
        chartsigma = 8;
        
    SimulateData(leveyjenningschart, numssampleintervals, chartmean, chartsigma);

    // Scale the y-axis of the X-Bar chart to display all data and control limits
    leveyjenningschart.autoScalePrimaryChartYRange();
    // Scale the y-axis of the Range chart to display all data and control limits
    leveyjenningschart.autoScaleSecondaryChartYRange();
    // Rebuild the chart using the current data and settings
    leveyjenningschart.rebuildChartUsingCurrentData();


}

export async function BuildEWMAChart(canvasid) {


    var htmlcanvas = document.getElementById(canvasid);
    var spccharttype = QCSPCChartTS.SPCControlChartData.EWMA_CHART; 
    var subgroupsize = 1;
    var numberpointsinview = 12;
    var charttitle = "EWMA Example";

    var ewmachart = QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchVariableControlChartChartTypeSubgroupSize(htmlcanvas, spccharttype, subgroupsize, numberpointsinview);

    ewmachart.setPreferredSize(800, 600);

    ewmachart.setGraphStopPosX(0.825);
    ewmachart.setGraphStartPosX(0.18);
    
    ewmachart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);
    ewmachart.getChartData().setEWMA_StartingValue(30);
    ewmachart.getChartData().setEWMA_Lambda(0.2);    
    ewmachart.setEnableScrollBar(true);
    ewmachart.setEnableDisplayOptionToggles(true);
    ewmachart.setEnableCategoryValues(true);
    ewmachart.setEnableCalculatedValues(true);
    ewmachart.setEnableAlarmStatusValues(true);
    ewmachart.setEnableChartToggles(true);
    ewmachart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    ewmachart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);
    var chartdata = ewmachart.getChartData();
    if (chartdata) {

        chartdata.setEWMA_Lambda(0.2);
        chartdata.setEWMA_StartingValue(30);        
        chartdata.setTitle (charttitle);
        chartdata.setChartDescriptor("EWMA");
    }


    var charttable = ewmachart.getChartTable();

    if (charttable)
    {
        charttable.setTableBackgroundMode(   QCSPCChartTS.SPCGeneralizedTableDisplay.TABLE_SINGLE_COLOR_BACKGROUND_GRIDCELL );
        charttable.setBackgroundColor1( QCSPCChartTS.ChartColor.WHITE);
        charttable.setBackgroundColor2( QCSPCChartTS.ChartColor.GRAY);
    }

    var numssampleintervals = 100;
    var chartmean = 30;
    var chartsigma = 5;

    SimulateData(ewmachart, numssampleintervals, chartmean, chartsigma);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    ewmachart.autoCalculateControlLimits();
        // Out some more data, different mean and sigma, to simulate out of control
        numssampleintervals = 50;
        chartmean = 32;
        chartsigma = 8;
        
    SimulateData(ewmachart, numssampleintervals, chartmean, chartsigma);
    // Scale the y-axis of the X-Bar chart to display all data and control limits
    ewmachart.autoScalePrimaryChartYRange();
    // Scale the y-axis of the Range chart to display all data and control limits
    ewmachart.autoScaleSecondaryChartYRange();
    // Rebuild the chart using the current data and settings
    ewmachart.rebuildChartUsingCurrentData();


}

export async function BuildMAMRChart(canvasid) {


    var htmlcanvas = document.getElementById(canvasid);
    var spccharttype = QCSPCChartTS.SPCControlChartData.MAMR_CHART; 
    var subgroupsize = 1;
    var numberpointsinview = 12;
    var charttitle = "MAMR Example";

    var mamrchart = QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchVariableControlChartChartTypeSubgroupSize(htmlcanvas, spccharttype, subgroupsize, numberpointsinview);

    mamrchart.setPreferredSize(800, 600);
    mamrchart.setGraphStopPosX(0.825);
    mamrchart.setGraphStartPosX(0.18);
    mamrchart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);

    mamrchart.setEnableScrollBar(true);
    mamrchart.setEnableDisplayOptionToggles(true);
    mamrchart.setEnableCategoryValues(true);
    mamrchart.setEnableCalculatedValues(true);
    mamrchart.setEnableAlarmStatusValues(true);
    mamrchart.setEnableChartToggles(true);
    mamrchart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    mamrchart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);
    mamrchart.getChartData().setTitle (charttitle);
    mamrchart.getChartData().setChartDescriptor("MAMR");
    mamrchart.getChartData().setMA_w(5);
    mamrchart.getPrimaryChart().setPlotMeasurementValues(true);
    var numssampleintervals = 100;
    var chartmean = 30;
    var chartsigma = 5;

    SimulateData(mamrchart, numssampleintervals, chartmean, chartsigma);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    mamrchart.autoCalculateControlLimits();

        // Out some more data, different mean and sigma, to simulate out of control
        numssampleintervals = 50;
        chartmean = 32;
        chartsigma = 8;
        
    SimulateData(mamrchart, numssampleintervals, chartmean, chartsigma);
    // Scale the y-axis of the X-Bar chart to display all data and control limits
    mamrchart.autoScalePrimaryChartYRange();
    // Scale the y-axis of the Range chart to display all data and control limits
    mamrchart.autoScaleSecondaryChartYRange();
    // Rebuild the chart using the current data and settings
    mamrchart.rebuildChartUsingCurrentData();


}

export async function BuildMAChart(canvasid) {


    var htmlcanvas = document.getElementById(canvasid);
    var spccharttype = QCSPCChartTS.SPCControlChartData.MA_CHART; 
    var subgroupsize = 1;
    var numberpointsinview = 12;
    var charttitle = "MA Example";

    var machart = QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchVariableControlChartChartTypeSubgroupSize(htmlcanvas, spccharttype, subgroupsize, numberpointsinview);

    machart.setPreferredSize(800, 600);
    machart.setGraphStopPosX(0.825);
    machart.setGraphStartPosX(0.18);
    machart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);

    machart.setEnableScrollBar(true);
    machart.setEnableDisplayOptionToggles(true);
    machart.setEnableCategoryValues(true);
    machart.setEnableCalculatedValues(true);
    machart.setEnableAlarmStatusValues(true);
    machart.setEnableChartToggles(true);
    machart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    machart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);
    machart.getChartData().setTitle (charttitle);
    machart.getChartData().setChartDescriptor("MA");
    machart.getChartData().setMA_w(5);
    machart.getPrimaryChart().setPlotMeasurementValues(true);
    var numssampleintervals = 100;
    var chartmean = 30;
    var chartsigma = 5;

    SimulateData(machart, numssampleintervals, chartmean, chartsigma);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    machart.autoCalculateControlLimits();

        // Out some more data, different mean and sigma, to simulate out of control
        numssampleintervals = 50;
        chartmean = 32;
        chartsigma = 8;
        
    SimulateData(machart, numssampleintervals, chartmean, chartsigma);
    // Scale the y-axis of the X-Bar chart to display all data and control limits
    machart.autoScalePrimaryChartYRange();
    // Scale the y-axis of the Range chart to display all data and control limits
    machart.autoScaleSecondaryChartYRange();
    // Rebuild the chart using the current data and settings
    machart.rebuildChartUsingCurrentData();


}

export async function BuildMAMSChart(canvasid) {


    var htmlcanvas = document.getElementById(canvasid);
    var spccharttype = QCSPCChartTS.SPCControlChartData.MAMS_CHART; 
    var subgroupsize = 1;
    var numberpointsinview = 12;
    var charttitle = "MAMS Example";

    var mamschart = QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchVariableControlChartChartTypeSubgroupSize(htmlcanvas, spccharttype, subgroupsize, numberpointsinview);

    mamschart.setPreferredSize(800, 600);
    mamschart.setGraphStopPosX(0.825);
    mamschart.setGraphStartPosX(0.18);
    mamschart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);

    mamschart.setEnableScrollBar(true);
    mamschart.setEnableDisplayOptionToggles(true);
    mamschart.setEnableCategoryValues(true);
    mamschart.setEnableCalculatedValues(true);
    mamschart.setEnableAlarmStatusValues(true);
    mamschart.setEnableChartToggles(true);
    mamschart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    mamschart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);
    mamschart.getChartData().setTitle (charttitle);
    mamschart.getChartData().setChartDescriptor("MAMS");
    mamschart.getChartData().setMA_w(5);
    mamschart.getPrimaryChart().setPlotMeasurementValues(true);
    var numssampleintervals = 100;
    var chartmean = 30;
    var chartsigma = 5;

    SimulateData(mamschart, numssampleintervals, chartmean, chartsigma);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    mamschart.autoCalculateControlLimits();

        // Out some more data, different mean and sigma, to simulate out of control
        numssampleintervals = 50;
        chartmean = 32;
        chartsigma = 8;
        
    SimulateData(mamschart, numssampleintervals, chartmean, chartsigma);
    // Scale the y-axis of the X-Bar chart to display all data and control limits
    mamschart.autoScalePrimaryChartYRange();
    // Scale the y-axis of the Range chart to display all data and control limits
    mamschart.autoScaleSecondaryChartYRange();
    // Rebuild the chart using the current data and settings
    mamschart.rebuildChartUsingCurrentData();


}

export async function BuildCusumChart(canvasid) {


    var htmlcanvas = document.getElementById(canvasid);
    var spccharttype  = QCSPCChartTS.SPCControlChartData.TABCUSUM_CHART; 
    var subgroupsize = 1;
    var numberpointsinview  = 12;
    var charttitle = "Cusum Example";
    var processMean  = 10;
    var kValue  = 0.5;
    var hValue  = 5;

    var cusumchart = 
       QCSPCChartTS.SPCBatchVariableControlChart.newSPCBatchCusumControlChart(htmlcanvas, spccharttype, subgroupsize, numberpointsinview,
        processMean, kValue, hValue);
	
    cusumchart.setPreferredSize(800, 600);
    cusumchart.setGraphStopPosX(0.825);
    cusumchart.setGraphStartPosX(0.18);
 
    cusumchart.setChartAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_SYMBOL);

    cusumchart.setEnableScrollBar(true);
    cusumchart.setEnableDisplayOptionToggles(true);
    cusumchart.setEnableCategoryValues(true);
    cusumchart.setEnableCalculatedValues(true);
    cusumchart.setEnableAlarmStatusValues(true);
    cusumchart.setEnableChartToggles(true);
    cusumchart.setTableAlarmEmphasisMode(QCSPCChartTS.SPCChartBase.ALARM_HIGHLIGHT_BAR);
    cusumchart.setHeaderStringsLevel( QCSPCChartTS.SPCControlChartData.HEADER_STRINGS_LEVEL1);
    var chartdata = cusumchart.getChartData();
    if (chartdata) {
      chartdata.setTitle (charttitle);
      chartdata.setChartDescriptor("Cusum");

    }


    var charttable = cusumchart.getChartTable()

    if (charttable)
    {
        charttable.getSampleItemTemplate().setDecimalPos( 2);
        charttable.getCalculatedItemTemplate().setDecimalPos(2);
    }    

    var primarychart = cusumchart.getPrimaryChart();

    SimulateDataCusum(cusumchart);


    // Calculate the SPC control limits for both graphs of the current SPC chart (X-Bar R)
    cusumchart.autoCalculateControlLimits();

    // Scale the y-axis of the X-Bar chart to display all data and control limits
    cusumchart.autoScalePrimaryChartYRange();

    // Rebuild the chart using the current data and settings
    cusumchart.rebuildChartUsingCurrentData();


}



function   SimulateDataCusum(spcchart)
    {

        var timestamp = new Date();
        var chartdata = spcchart.getChartData();
        if (!chartdata) return;
        var i= 0;
        for ( i=0; i < Ewma8_2Data.length; i++)
        {
            
            // Use the ChartData sample simulator to make an array of sample data
            var samples = QCSPCChartTS.DoubleArray.newDoubleArrayN(1);
            samples.setElement(0, Ewma8_2Data[i]);
            // Add the new sample subgroup to the chart
            chartdata.addNewSampleRecordBatchNumberDateSamples(i,timestamp, samples);
            // increment simulated time by timeincrementminutes minutes
            QCSPCChartTS.ChartCalendar.add(timestamp, QCSPCChartTS.ChartConstants.MINUTE, 15);				
        }
    }


function SimulateData(spcchart, count, mean, sigma) {
    // batch number for a given sample subgroup
    var batchCounter = 0;
    var i;
    var timestamp = new Date();
    if (!spcchart) return;
    if (!spcchart.getChartData()) return;
    
    var currentcount = spcchart.getChartData().getCurrentNumberRecords();
    if (currentcount > 0) // start date at the previous ending date plut time increment
    {
        var ts  = spcchart.getChartData().getTimeValue(currentcount-1);
        timestamp = ts;
        QCSPCChartTS.ChartCalendar.add(timestamp, QCSPCChartTS.ChartConstants.MINUTE, 15);
    }
    for (i = 0; i < count; i++) {
        // Simulate a sample subgroup record
        var samples = spcchart.getChartData().simulateMeasurementRecordMeanRange(mean, sigma);
        // Update chart data using i as the batch number
        batchCounter = currentcount + i;
        var note = "";
        if ((i % 5) == 0) note = "This is a note";
        // Add a new sample record to the chart data
        spcchart.getChartData().addNewSampleRecordBatchNumberDateSamplesNotes(batchCounter, timestamp, samples, note);
        // Simulate passage of timeincrementminutes minutes
        QCSPCChartTS.ChartCalendar.add(timestamp, QCSPCChartTS.ChartConstants.MINUTE, 15);
    }

}

