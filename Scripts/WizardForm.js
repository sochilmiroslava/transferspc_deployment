﻿function DisplayStep() {
    var selectedStep = null;
    var firstInputError = $("input.input-validation-error:first"); // check for any invalid input fields
    if (firstInputError.length) {
        selectedStep = $(".wizard-confirmation");
        if (selectedStep && selectedStep.length) { // the confirmation step should be initialized and selected if it exists
            UpdateConfirmation();
        }
        else {
            selectedStep = firstInputError.closest(".wizard-step"); // the first step with invalid fields should be displayed
        }
    }
    if (!selectedStep || !selectedStep.length) {
        selectedStep = $(".wizard-step:first"); // display first step if no step has invalid fields
    }

    $(".wizard-step:visible").hide(); // hide the step that currently is visible
    selectedStep.fadeIn(); // fade in the step that should become visible

    // enable/disable the prev/next/submit buttons
    if (selectedStep.prev().hasClass("wizard-step")) {
        $("#wizard-prev").show();
    }
    else {
        $("#wizard-prev").hide();
    }
    if (selectedStep.next().hasClass("wizard-step")) {
        // $("#wizard-submit").hide();
        $("#wizard-next").show();
    }
    else {
        $("#wizard-next").hide();
        //$("#wizard-submit").show();
    }
}

function PrevStep() {
    $("#Data_Count").val(1);
    var currentStep = $(".wizard-step:visible"); // get current step

    if (currentStep.prev().hasClass("wizard-step")) { // is there a previous step?

        currentStep.hide().prev().fadeIn();  // hide current step and display previous step

        //$("#wizard-submit").hide(); // disable wizard-submit button
        $("#wizard-next").show(); // enable wizard-next button

        if (!currentStep.prev().prev().hasClass("wizard-step")) { // disable wizard-prev button?
            $("#wizard-prev").hide();
        }
    }
}

function NextStep() {
    $("#Data_Count").val(2);
    var currentStep = $(".wizard-step:visible"); // get current step

    var validator = $("form").validate(); // get validator
    var valid = true;
    currentStep.find("input:not(:blank)").each(function () { // ignore empty fields, i.e. allow the user to step through without filling in required fields
        if (!validator.element(this)) { // validate every input element inside this step
            valid = false;
        }
    });
    if (!valid)
        return; // exit if invalid

    if (currentStep.next().hasClass("wizard-step")) { // is there a next step?

        if (currentStep.next().hasClass("wizard-confirmation")) { // is the next step the confirmation?
            UpdateConfirmation();
        }

        currentStep.hide().next().fadeIn();  // hide current step and display next step

        $("#wizard-prev").show(); // enable wizard-prev button

        if (!currentStep.next().next().hasClass("wizard-step")) { // disable wizard-next button and enable wizard-submit?
            $("#wizard-next").hide();
            //  $("#wizard-submit").show();
        }
    }
}

function Submit() {

    //Aqui creo la variable de control de subida
    var result = true;
    var Data_Count = $("#Data_Count").val();

    //Borro todos los errores
    $('span[id^="genericError"]').remove();
    if (Data_Count == 2)//Aki compruebo que estan agregando sucursales o no
    {

        ////////////////////////////////////// Valido 2do Paso Contacto ////////////////////////////////////////////////
        var name = $('#Suc_Nombre').val();
        if (name == "") {
            $('#Div_Suc_Nombre').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este Campo es Obligatorio</span>');
            result = false;
        }
        var calleSuc = $('#Suc_Calle').val();
        if (calleSuc == "") {
            $('#Div_Suc_Calle').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este Campo es Obligatorio</span>');
            result = false;
        }

        //Valido Cont_Telefono
        if (($('#Cont_Telefono').val() == "")) {
            $('#Div_Cont_Telefono').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
            result = false;
        }

        //Valido Cont_Cargo
        if (($('#Cont_Cargo').val() == "")) {
            $('#Div_Cont_Cargo').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
            result = false;
        }

        //Valido Cont_Fecha
        if (($('#Cont_Fecha').val() == "")) {
            $('#Div_Cont_Fecha').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
            result = false;
        }




        var Email1 = $('#Suc_Email').val();

        if (Email1 == "") {
            $('#Div_Suc_Email').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este Campo es Obligatorio</span>');
            result = false;
        } else if (Email1 != "") {//Valido el Email

            var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!expr.test($('#Suc_Email').val())) {
                $('#Div_Suc_Email').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este valor debe ser un correo válido</span>');
                result = false;
            }
        }
    }
    else {
        ////////////////////////////////////// Valido Primer Paso ////////////////////////////////////////////////
        //Valido el RUT
        if (($('#RUT').val() == "") || (!/^([0-9])*$/.test($('#RUT').val()))) {
            $('#Div_RUT').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este valor debe ser un numero</span>');
            result = false;
        }

        //Valido Razon Social
        if (($('#RazonSocial').val() == "")) {
            $('#Div_RazonSocial').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
            result = false;
        }

        //Valido NombreFantasia
        if (($('#Nombre_Fantasias').val() == "")) {
            $('#Div_NombreFantasia').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
            result = false;
        }

        ////Valido Barrio
        //if ($('#Barrios_Type').val() == 0) {
        //    indiceBarrio = ($('#Barrios_Id_Selected').val());
        //    if (indiceBarrio == null || indiceBarrio == -1 || indiceBarrio == "") {
        //        $('#Div_Barrio').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Debe seleccionar uno</span>');
        //        result = false;
        //    }
        //}
        //else {
        //    //Valido el barrrio texto
        //    if ($('#Txt_Barrio').val() == "") {
        //        $('#Div_Barrio').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este Valor es obligatorio</span>');
        //    }
        //}

        //Valido DireccionCalle
        if (($('#DireccionCalle').val() == "")) {
            $('#Div_DireccionCalle').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
            result = false;
        }

        var Email2 = $('#Email').val();
        //Valido el Email
        if (Email2 == "") {
            $('#Div_Email').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este Campo es Obligatorio</span>');
            result = false;
        } else if (Email2 != "") {

            expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!expr.test(Email2)) {
                $('#Div_Email').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este valor debe ser un correo válido</span>');
                result = false;
            }
        }


        ////Valido Web
        //if (($('#Web').val() == "")) {
        //    $('#Div_Web').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
        //    result = false;
        //}

        //Valido el DireccionNro
        if (($('#DireccionNro').val() == "") || (!/^([0-9])*$/.test($('#DireccionNro').val()))) {
            $('#Div_DireccionNro').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este valor debe ser un numero</span>');
            result = false;
        }

        //Valido Departamento
        indiceDepartamento = ($('#Departamento').val());
        if (indiceDepartamento == null || indiceDepartamento == 0) {
            $('#Div_Departamento').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Debe seleccionar uno</span>');
            result = false;
        }

        //Valido Localidad
        indiceLocalidad = ($('#Localidad').val());
        if (indiceLocalidad == null || indiceLocalidad == 0) {
            $('#Div_Localidad').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Debe seleccionar uno</span>');
            result = false;
        }

    }




    if (result) {

        $("form").submit();
    }

}





$(function () {

    // attach click handlers to the nav buttons
    $("#wizard-prev").click(function () { PrevStep(); });
    $("#wizard-next").click(function () {

        //Aqui creo la variable de control de primer Step
        var result = true;

        //Borro todos los errores
        $('span[id^="genericError"]').remove();

        ////////////////////////////////////// Valido Primer Paso ////////////////////////////////////////////////

        //Valido el RUT
        if (($('#RUT').val() == "") || (!/^([0-9])*$/.test($('#RUT').val()))) {

            $('#Div_RUT').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este valor debe ser un numero</span>');
            result = false;
        }

        //Valido Razon Social
        if (($('#RazonSocial').val() == "")) {
            $('#Div_RazonSocial').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
            result = false;
        }

        //Valido NombreFantasia
        if (($('#Nombre_Fantasias').val() == "")) {
            $('#Div_NombreFantasia').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
            result = false;
        }

        //Valido Barrio
        if ($('#Barrios_Type').val() == 0) {
            indiceBarrio = ($('#Barrios_Id_Selected').val());
            if (indiceBarrio == null || indiceBarrio == -1 || indiceBarrio == "") {
                $('#Div_Barrio').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Debe seleccionar uno</span>');
                result = false;
            }
        }
        else {
            //Valido el barrrio texto
            if ($('#Txt_Barrio').val() == "") {
                $('#Div_Barrio').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este Valor es obligatorio</span>');
            }
        }

        //Valido DireccionCalle
        if (($('#DireccionCalle').val() == "")) {
            $('#Div_DireccionCalle').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
            result = false;
        }

        var Email3 = $('#Email').val();

        if (Email3 == "") {
            $('#Div_Email').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este Campo es Obligatorio</span>');
            result = false;
        } else if (Email3 != "") {//Valido el Email

            var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!expr.test($('#Email').val())) {
                $('#Div_Email').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este valor debe ser un correo válido</span>');
                result = false;
            }
        }
        ////Valido Web
        //if (($('#Web').val() == "")) {
        //    $('#Div_Web').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este campo es obligatorio</span>');
        //    result = false;
        //}

        //Valido el DireccionNro
        if (($('#DireccionNro').val() == "") || (!/^([0-9])*$/.test($('#DireccionNro').val()))) {
            $('#Div_DireccionNro').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Este valor debe ser un numero</span>');
            result = false;
        }

        //Valido Departamento
        indiceDepartamento = ($('#Departamento').val());
        if (indiceDepartamento == null || indiceDepartamento == 0) {
            $('#Div_Departamento').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Debe seleccionar uno</span>');
            result = false;
        }

        //Valido Localidad
        indiceLocalidad = ($('#Localidad').val());
        if (indiceLocalidad == null || indiceLocalidad == 0) {
            $('#Div_Localidad').append('<span id="genericError" class="field-validation-error text-danger" data-valmsg-for="Usuario" data-valmsg-replace="true"> Debe seleccionar uno</span>');
            result = false;
        }


        if (result) {

            NextStep();
        }




    });
    $("#AddCliente-submit").click(function () {

        Submit();

    });

    // display the first step (or the confirmation if returned from server with errors)
    DisplayStep();
});

